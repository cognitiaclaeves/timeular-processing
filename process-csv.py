import csv
import click

# Need to track hours spent on:
# - #opi 
# - #opi-cli.@client
# - field for #opi > 40 hours
# - field for #opi-cli.@client > 40 hours
# - extra fields needs to be added for carry-over, for:
# - rolling #opi overage
# - #opi-cli.@client can't roll over (must be banked or billed)


class ClickContextData(object):
	''' Custom object works with Click to pass context to sub-commands '''

	''' Define a class for the click context to avoid context confusion 
		in case this click command is nested with others later
		While this is not likely, the context is needed, so might as well
		take the extra step. '''

	def __init__(self, **kwargs):

		self.kwargs = kwargs
		self.csv_filename = kwargs.get('csv_filename')
		self.csv_data = None
		self.processed_data = None


	def load_csv(self):

		with open(fl, newline='') as f:
			 reader = csv.reader(f)
			 self.csv_data = list(reader)


	def set_processed_data(processed_data):

		self.processed_data = processed_data



class Timeular_Data_Processor(object):

	''' massages data to add calculations for groups in processed_data '''

	def __init__(self, context_object):

		self.context = context_object
		self.data = []
		self.process_data()
		self.context.set_timeular_data()

	def set_custom_fields(self):
		pass

	def process_data(self):

		self.context.set_processed_data(self.data)


class CSV_Mangler(object):

	''' Adds fields/formulas? to CSV for tracking '''



pass_clickcontextdata = click.make_pass_decorator(ClickContextData)


# accepted_clusters = ['.','qa','staging','dr', 'production', 'beta', 'demo', 'test-qa', 'development']
# accepted_environments = ['qa', 'staging','dr', 'production', 'beta', 'demo', 'test-qa', 'development']
# # environment as defined by environment tag in AWS account

@click.group()
@click.option('--debug/--no-debug', default=False)
# @click.option('--dry-run', default=False, is_flag=True, help='Show command without making changes')
# @click.option('--skip247', default=False, is_flag=True, help='Skip interaction with site24x7')
# @click.option('-r', '--region', default='us-west-1', help='AWS region', envvar='BUCKET_REG')

@click.argument('csv_filename')

# @click.argument('cluster_name', type=click.Choice(accepted_clusters, case_sensitive=False))
# @click.argument('environment', type=click.Choice(accepted_environments, case_sensitive=False))

# # Going to need to switch accounts based on clusters ... 

# @click.pass_context
# def cli(ctx, cluster_name, region, environment, dry_run, skip247):
# 	ctx.obj = UpDown(cluster_name = cluster_name, region = region, 
# 					 environment = environment, dry_run = dry_run, 
# 					 skip247 = skip247
# 					 )


@click.pass_context
def cli(ctx, debug, csv_filename):
	ctx.obj = ClickContextData(debug=debug, csv_filename=csv_filename)



@cli.command()
@pass_clickcontextdata
def load_csv(obje):
	print(obje.csv_filename)

# @click.option('--minimal', default=False, is_flag=True, help='Start ASG with only one instance')
# # @click.option('--skip-lt', default=False, is_flag=True, help='Pass for ECS configurations with no launch template')
# def up(obje, minimal): # , skip_lt):
# 	''' bring the entire cluster up '''

# 	kwargs = obje.kwargs

# 	obje.requested_state = 'up'



# >>> fl
# '2021-06-16_2021-06-30_report.csv'
# >>> with open(fl, newline='') as f:
# ...     reader = csv.reader(f)
# ...     data = list(reader)

if __name__ == '__main__':
	cli()
